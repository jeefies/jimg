# JIMG

**Jeefy's Image processor**

*Only Support jpeg images*

[中文版本](./README-zh_cn.md)

## Dependencies
- libjpeg
- ...

## Tricks

Use Segment tree to merge near colors.

> Ways to tell whether the color is near with each other:
>
> LAB color fields: use $\overline{r}, \Delta R, \Delta G, \Delta B$
>
> $\Delta C = \sqrt{(2 + \frac {\overline{r}} {256}) * \Delta R^2 + 4 * \Delta G^2 + (2 + \frac {255 - \overline{r}} {256}) * \Delta B^2}$
> 

This means, when Color.diff(Color) ($\le 0$) is smaller, the colors are more similar.

## Methods

There are some operations implement of images:

- NoiseReducer. Use simplest algorithm: use average color around the point.
- PixelMerger. Merge similar colors. Can draw back in different shapes(rectangle or ellipse or user-defined...)

See `jimage.d` (the console application) for exmaple.

## Console Applications

`jimage` can process a single file.

`jmulti` can process all files under some folders. More see `cmds.sh` to use it with ffmpeg.

## Optimize

I cannot optimize the program more.

I tried using multi-processing but it slowed down the program.

I don't know what (maybe I wrote the multi-processing wrongly..) causes it.

If you can have any idea (or practical optimize code), please contact me by email: **jeefy163@163.com** or **jeefyol@outlook.com**。

I would be really appreciative about it.
