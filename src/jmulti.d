import std.stdio, std.file, std.path;
import  std.getopt;
import std.algorithm, std.array;
import core.thread, core.sync.mutex;
import core.atomic;
import std.parallelism;
import std.datetime.stopwatch;

import jimg;

enum Shape {
	rect, ellipse
};

double dbound = 75;
Shape shape = Shape.rect;
int blur = 1;

string[] listdir(string path) {
	return dirEntries(path, SpanMode.shallow)
		.filter!(a => a.isFile && (extension(a.name) == ".jpeg" || extension(a.name) == ".jpg"))
		.map!((return a) => a.name)
		.array;
}

class Lock {}
shared Lock lock = new shared Lock();
shared Lock rdLock = new shared Lock(), wtLock = new shared Lock();

shared string[] files;
shared int index = 0, tot = 0;

void worker() {
	bool done = false;

	auto reducer = new NoiseReducer(blur);
	auto merger = new PixelMerger(dbound,
		defaultSpliter,
		cast(IColorFiller)(shape == Shape.rect ? rectangleFiller : ellipseFiller),
	);

	while (!done) {
		string cur;
		synchronized (lock) {
			if (index == files.length) break;
			cur = files[index];
			atomicOp!"+="(index, 1);
		}

		Image img;
		synchronized (rdLock) {
			img = new Image(cur);
		}

		if (blur > 1)
			img = reducer.denoise(img);
		img = merger.merge(img);

		synchronized (wtLock) {
			img.write(stripExtension(cur) ~ "-merged.jpeg");
			atomicOp!"+="(tot, 1);
			if (tot % 10 == 0) writef("\rAlready proc %d images", tot);
		}

	}
}

int main(string[] args) {
	string[] dirs;
	// vi(ga, vi(ga-2,
	auto parser = getopt(
		args,
		"blur|b",    "set the denoising level when preprocessing the image (default 1 for lowest)", &blur,
		"diff|d",    "set how similar the color would be merged with each. (default 75)",           &dbound,
		"shape|s",   "rect for rectangle, ellipse for ellipse (default rect)",                      &shape,
	);

	if (parser.helpWanted) {
		defaultGetoptPrinter("Usage: jimg [OPTIONS] [PATH...]",
			parser.options);
		return 0;
	}

	foreach (dir; args[1 .. $]) {
		if (dir.exists && dir.isDir)
			files ~= listdir(dir);
		else
			stderr.writeln("Not a dir: ", dir);
	} 

	// writeln(files);

	stderr.writefln("Blur level: %d\nfill shape: %s\ncolor difference bound: %s", blur, shape, dbound);

	stderr.writeln("Start processing:");
	auto sw = StopWatch(AutoStart.yes);

	ThreadGroup tg = new ThreadGroup();
	for (int i = 0; i < 50; ++i) {
		tg.create(() => worker());
	}

	tg.joinAll();
	scope(exit) writefln("\nProc %d images use time: %s", files.length, sw.peek());

	return 0;
}
