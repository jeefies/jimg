module jimg.image;

import std.stdio, std.file, std.format;
import std.datetime.stopwatch : AutoStart, StopWatch;
import core.memory;

import jimg;

alias CImage = int;
extern (C) {
	// Image Processor Interface (with libjpeg)
	CImage imgWOpen(FILE*, int quality);
	CImage imgROpen(FILE*);
	CImage imgRaw(ubyte* raw, ulong);
	void imgClose(CImage);
	void imgNoFree(CImage);
	uint imgWidth(CImage imgID);
	uint imgHeight(CImage imgID);
	uint imgComponents(CImage imgID);
	int imgRPixels(CImage imgID, ubyte* raw);
	int imgWPixels(CImage imgID, uint h, uint w, ubyte* raw);

	// Image Shower
	int showImage(int, int, int*);
}

class Image {
	private uint _width, _height;
	Color[][] pixels;

	this(in int h, in int w) {
		_width = w, _height = h;
		pixels = new Color[][](h, w);
	}

	this(in Image src) {
		_width = src._width, _height = src._height;
		pixels = new Color[][](_height, _width);
		for (uint i = 0; i < _height; ++i) {
			for (uint j = 0; j < _width; ++j)
				pixels[i][j] = src.pixels[i][j];
		}
	}

	this(ubyte[] jpeg_raw) {
		CImage imgID = imgRaw(jpeg_raw.ptr, jpeg_raw.length);
		scope (exit) imgClose(imgID);
		if (imgID < 0) {
			return;
		} init(imgID);
	}

	this(string fn) {
		File ifp = File(fn, "rb");
		scope(exit) ifp.close();
		
		CImage imgID = imgROpen(ifp.getFP());
		if (imgID < 0) {
			return;
		} init(imgID);

		imgNoFree(imgID);
		imgClose(imgID);
	}

	void init(CImage imgID) {
		uint h = _height = imgHeight(imgID), 
			 w = _width = imgWidth(imgID), 
			 c = imgComponents(imgID);

		ubyte[] raw = new ubyte[](h * w * c);
		scope(exit) destroy(raw);
		imgRPixels(imgID, raw.ptr);


		pixels = new Color[][](h, w);
		for (uint i = 0; i < h; ++i) {
			auto row = pixels[i];
			for (uint j = 0, offset = i * w * c; j < w; ++j) {
				if (c == 1) {
					row[j].init(raw[offset + j], raw[offset + j], raw[offset + j]);
				} else {
					row[j].init(raw[offset + j * c], raw[offset + j * c + 1], raw[offset + j * c + 2]);
				}
			}
		}
	}

	~this() {
		destroy(pixels);
	}

	@property uint height() {
		return _height;
	}

	@property uint width() {
		return _width;
	}

	void show() {
		int[] ci = new int[](width * height);
		scope(exit) destroy(ci);
		for (uint i = 0; i < height; ++i) for (uint j = 0; j < width; ++j)
			ci[i * width + j] = pixels[i][j].toInt();
		showImage(height, width, ci.ptr);
	}

	void write(string fn) {
		File of = File(fn, "w");
		scope(exit) of.close();
		write(of);
	}

	void write(File of) {
		uint w = width, h = height;

		// use <0.03ms
		ubyte* raw = cast(ubyte *)GC.malloc(w * h * 3);
		scope(exit) GC.free(raw);

		// imgWOpen use <0.03ms
		CImage imgID = imgWOpen(of.getFP(), 100);
		scope (exit) {
			imgNoFree(imgID);
			imgClose(imgID);
		}

		// This part use about 50ms (3318 x 1536)...
		uint base = 0;
		for (uint i = 0; i < h; ++i) for (uint j = 0; j < w; ++j, base += 3) {
			raw[base + 0] = cast(ubyte)(pixels[i][j].R),
			raw[base + 1] = cast(ubyte)(pixels[i][j].G),
			raw[base + 2] = cast(ubyte)(pixels[i][j].B);
		}

		// imgWPixels use about 23ms
		if (imgID < 0) {
			return;
		} else imgWPixels(imgID, h, w, raw);
	}

	void prtPixels() {
		for (int i = 0; i < height(); ++i) {
			for (int j = 0; j < width(); j++) {
				writef("%06x ", pixels[i][j].toInt());
			} writeln();
		}
	}
};
