module jimg.color;

import std.math, std.format;

// show the color by SDL (written in C++)
extern (C) int showColor(int, int, int);

struct Color {
	int R, G, B;
	this(in int r, in int g, in int b) {
		init(r, g, b);
	}

	void init(in int r, in int g, in int b) {
		R = r, G = g, B = b;
	}

	void add(in Color c) {
		R += c.R, G += c.G, B += c.B;
	}

	Color division(ulong div) {
		R /= div, G /= div, B /= div;
		return this;
	}

	/*
	   Use LAB color fields to see wether two color is similar or not.
	 */
	double diff(in Color oth) {
		double aR = fabs(cast(double)(R - oth.R) / 2);
		double dR = (R - oth.R), dG = (G - oth.G), dB = (B - oth.B);
		return sqrt((2 + aR / 256) * dR * dR + 4 * dG * dG + (2 + (255 - aR) / 256) * dB * dB);
	}

	int toInt() {
		return ((R & 0xFF) << 16) | ((G & 0xFF) << 8) | (B & 0xFF);
	}

	void show() {
		showColor(R, G, B);
	}

	string toString() {
		return format("%06x", this.toInt());
	}
};
