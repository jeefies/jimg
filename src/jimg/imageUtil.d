module jimg.imageUtil;

import std.stdio, std.format;
import std.parallelism;

import std.datetime.stopwatch;

import jimg;

struct Area {
	// left, right, up and down boundaries.
	int l, r, u, d;
	this(in int _l, in int _r, in int _d, in int _u) {
		l = _l, r = _r, d = _d, u = _u;
	}

	pragma(inline) bool isValid() {
		return l <= r && d <= u;
	}

	pragma(inline) int size() {
		return (r - l + 1) * (u - d + 1);
	}

	pragma(inline) string toString() {
		return format("Area{[%d, %d], [%d, %d]}", l, r, d, u);
	}
}

/*
   interface for filling a specific color in certain space of a image.
   Take the RectangleFiller for example.
 */
interface IColorFiller {
	void opCall(ref Image img, in Area a, in Color c);
}

RectangleFiller rectangleFiller;
class RectangleFiller : IColorFiller {
	static this() {
		rectangleFiller = new RectangleFiller();
	}

	pragma(inline)
	void opCall(ref Image img, in Area a, in Color c) {
		for (int i = a.d; i <= a.u; ++i) {
			for (int j = a.l; j <= a.r; ++j)
				img.pixels[i][j] = c;
		}
	}
}

EllipseFiller ellipseFiller;
class EllipseFiller : IColorFiller {
	static this() {
		ellipseFiller = new EllipseFiller();
	}

	pragma(inline)
	void opCall(ref Image img, in Area a, in Color c) {
		double ew = cast(double)(a.r - a.l) / 2, eh = cast(double)(a.u - a.d) / 2;
		double cx = cast(double)(a.r + a.l) / 2, cy = cast(double)(a.u + a.d) / 2;
		double qa = ew * ew, qb = eh * eh;
		bool delegate(double, double) filter = (x, y) => qb * x * x + qa * y * y - qa * qb <= 0;

		for (int i = a.d; i <= a.u; ++i) {
			for (int j = a.l; j <= a.r; ++j)
				if (filter(j - cx, i - cy)) img.pixels[i][j] = c;
		}
	}
}

/*
   ISpliter is the interface for PixelMerger. For spliting an Area into several parts.
   Take the DefaultSpliter for example.
 */
interface ISpliter {
	// res would be preallocated according to maxlength() !!!
	void opCall(in Area a, Area[] results);
	// maxlength should return the max length of the array which opCall returns.
	// for example, defaultSpliter.maxlength() returns 4
	int maxlength();
}

DefaultSpliter defaultSpliter;
class DefaultSpliter : ISpliter {
	static this() {
		defaultSpliter = new DefaultSpliter();
	}

	pragma(inline)
	void opCall(in Area a, Area[] results) const {
		int hmid = (a.l + a.r) >> 1, vmid = (a.d + a.u) >> 1;
		results[0] = Area(a.l, hmid, a.d, vmid); // left up
		results[1] = Area(hmid + 1, a.r, a.d, vmid); // right up
		results[2] = Area(a.l, hmid, vmid + 1, a.u); // left down
		results[3] = Area(hmid + 1, a.r, vmid + 1, a.u); // right down
	}

	pragma(inline) int maxlength() {
		return 4;
	}
}

/*
   PixelMerger is a class for merging similar pixels.
   diffBound stands for how similar the merged color should be.

NOTICE: for some reason, it's better to denoise the image (by NoiseReducer) at first
	to make the merged image looks better.
 */
class PixelMerger {
	private Image hot;
	private MemPool!Area apool;
	private MemPool!Color cpool;

	double diffBound;
	ISpliter spliter;
	IColorFiller filler;

	this(double _diffBound = 100, ISpliter _spliter = defaultSpliter, IColorFiller _filler = rectangleFiller) {
		spliter = _spliter, diffBound = _diffBound, filler = _filler;
		apool = new MemPool!Area(defaultSpliter.maxlength());
		cpool = new MemPool!Color(defaultSpliter.maxlength());
	}

	~this() {
		destroy(apool);
		destroy(cpool);
	}
	
	Image merge(Image img) {
		uint w = img.width(), h = img.height();
		hot = img;
		Image result = new Image(h, w);

		Color _tmp;
		Area whole = Area(0, w - 1, 0, h - 1);

		if (merge(whole, _tmp, result)) {
			// Fix if need to merge the whole image.
			filler(result, whole, _tmp);
		}

		return result;
	}

	/*
	   Let's consider the complexity of the method.
	   We go down for \log{wh} levels. And we know that:
	   T(n) = 4T(n / 4) + O(1)
	   so this part has T(n) = O(log n)
	   And when flush we use O(wh).
	   So totally we just use the time depends on the size of the image linearly.
	   
	   But why the use average 1100ms to process test-small.jpeg ?

	   Update At 2023.06.25:
	   	test under test-small.jpeg (delete in some commit)
	   	Add pragma(inline), time to 1000ms.
		Use fixed length array for cls and cla, time to 500ms.
		Use mempool to allocate cls and cla, time to 200ms.
		Rewrite ISpliter interface and use pre-allocated mem (by mempool), time to 150ms!
		!! Great step forward
	
	   Update At 2023.06.26:
	   	It's strange that parallelism would slower down the speed of the program (maybe I'm not good in it).
		Testing results can be seen in the commends followed the codes below.

	   Update At 2023.06.29:
	   	test under test.jpeg.
		See branch async_merger.
		I tried to use `spawn` to optimzie filling part.
		However, it slowed down the program to 850ms ~ 1050ms (original about 750ms).
		Confused...
	*/
	bool merge(Area cur, out Color c, ref Image result) {
		if (!cur.isValid())
			return false;
		if (cur.size() == 1) {
			c = hot.pixels[cur.u][cur.l];
			return true;
		}

		int claID, clsID, lowID;
		auto cla = apool.pull(claID), low = apool.pull(lowID);
		auto cls = cpool.pull(clsID);
		scope(exit) {
			apool.recycle(lowID, claID);
			cpool.recycle(clsID);
		}

		int length = 0, canmerge = 1;
		Color avc = Color(0, 0, 0);
		spliter(cur, low);
		foreach (a; low) {
			if (!a.isValid()) 
				continue;

			if (!merge(a, cls[length], result)) {
				canmerge = false;
				continue;
			}

			cla[length] = a;
			avc.add(cls[length++]);
		}

		if (length == 0) {
			return canmerge = false;
		} else c = avc.division(length);

		for (int i = 0; i < length; ++i) {
			if (avc.diff(cls[i]) > diffBound) canmerge = false;
		}

		// this would use about 700ms
		// just doing this at most take up to 200ms...
		// however, whem I'm testing using parallel here,
		// the result shows that it takes 1s500ms to fill...
		// That's really strange
		// And change filler would have similar results...
		if (!canmerge) for (int i = 0; i < length; ++i) {
			filler(result, cla[i], cls[i]);
		}

		return cast(bool)canmerge;
	}
}

struct Range {
	int i, limit;

	pragma(inline) bool empty() {
		return limit <= i;
	}

	pragma(inline) int front() const {
		return i;
	}

	pragma(inline) void popFront() {
		++i;
	}
}

class NoiseReducer {
	// kernel size (ksize * kszie) area (auto adjust to odd number);
	private Image hot;

	int ksize;

	this(int ks = 3) {
		ksize = ks / 2 * 2 + 1;
	}

	@property int hksize() {
		return ksize / 2;
	}

	pragma(inline)
	Color sumAt(uint x, uint y) {
		Color sum = Color(0, 0, 0);
		int hk = hksize;
		for (uint i = x - hk, ie = x + hk; i <= ie; ++i) {
			for (uint j = y - hk, je = y + hk; j <= je; ++j)
				sum.add(hot.pixels[i][j]);
		} sum.division(ksize * ksize);
		return sum;
	}

	Image denoise(Image img) {
		hot = img;

		uint w = img.width(), h = img.height(), hk = cast(uint)hksize;
		Image res = new Image(h - ksize + 1, w - ksize + 1);

		auto range = Range(hk, h - hk);
		// use about 450ms
		// for (int i = hk; i < h - hk; ++i) {
		// use about 170ms in (3315 x 1536)
		foreach (i; parallel(range, 75)) {
			for (uint j = hk; j + hk < w; ++j) {
				res.pixels[i - hk][j - hk] = sumAt(i, j);
			}
		}

		return res;
	}
}
