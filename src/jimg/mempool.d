module jimg.mempool;

import std.stdio;

private class Lock {}

class MemPool(T) {
	private T[] pool;
	// ilen is the block size of each item.
	private int ilen, use;
	private int free_size;
	private int[] free;

	this(int len) {
		ilen = len;
		use = free_size = 0;
		free.length = 2;
	}

	~this() {
		destroy(free);
		destroy(pool);
	}

	pragma(inline)
	T[] pull(out int id) {
		if (free_size) {
			id = free[--free_size];
		} else {
			if (pool.length == use * ilen) {
				pool.length = use ? use * ilen * 2 : ilen * 4;
			} id = use++;
		}

		return pool[id * ilen .. id * ilen + ilen];
	}

	pragma(inline)
	void recycle(int[] ids ...) {
		foreach (id; ids) {
			if (free.length == free_size) {
				free.length = free_size * 2;
			} free[free_size++] = id;
		}
	}
}
