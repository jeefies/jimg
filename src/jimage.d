import std.stdio, std.string;
import std.file;
import std.getopt, std.experimental.logger;
import std.datetime.stopwatch : StopWatch, AutoStart;

import jimg;

bool verbose = false;
Logger jlogger;

enum Shape {
	rect, ellipse
};

ubyte[] readIn(string ifn) {
	File fp;
	if (ifn == "") {
		jlogger.log("Image data read from stdin:");
		fp = stdin;
	} else {
		jlogger.log("Image data read from file %s", ifn);
		fp = File(ifn, "rb");
	}

	ubyte[] data;
	foreach (ubyte[] buffer; chunks(fp, 4096)) {
		data ~= buffer;
	}

	jlogger.logf("Image raw data read: %d bytes", data.length);
	return data;
}

void main(string[] args) {
	string ifn = "", ofn = "", logfn = "";
	double dbound = 75;
	Shape shape = Shape.rect;
	int blur = 1;

	bool show = false;

	// vi(ga, vi(ga-2,
	auto parser = getopt(
		args,
		"infile|i",  "specific the file to read, or stdin default",                                 &ifn,
		"outfile|o", "specific the file to write",                                                  &ofn,
		"logfile",   "specific the file log write in, default stderr",                              &logfn,
		"blur|b",    "set the denoising level when preprocessing the image (default 1 for lowest)", &blur,
		"diff|d",    "set how similar the color would be merged with each. (default 75)",           &dbound,
		"shape|s",   "rect for rectangle, ellipse for ellipse (default rect)",                      &shape,
		"show",      "show the final proceeded image at once",                                      &show,
		"verbose|v", "show running message (use log file instead)",                                 &verbose,
	);

	if (parser.helpWanted) {
		defaultGetoptPrinter("Usage: jimg [OPTIONS] [-f FILE]",
			parser.options);
		return;
	}

	if (logfn != "") {
		auto logf = File(logfn, "w");
		jlogger = new FileLogger(logf);
		jlogger.infof("Create logger write in %s", logfn);
	} else {
		jlogger = new FileLogger(stderr);
		jlogger.info("Create logger write in stdin");
	}

	auto sw = StopWatch(AutoStart.yes);
	auto from = sw.peek();

	ubyte[] inData = readIn(ifn);

	jlogger.info("Read in data use time: ", sw.peek() - from);
	from = sw.peek();

	Image img = new Image(inData);

	jlogger.info("new Image from in Data use time: ", sw.peek() - from);
	from = sw.peek();

	if (blur > 1) {
		NoiseReducer reducer = new NoiseReducer(blur);
		img = reducer.denoise(img);

		jlogger.logf("Denoise image with kernel size %d", blur);
	}

	jlogger.info("After denoising (if denoised) use time: ", sw.peek() - from);

	IColorFiller filler = shape == Shape.rect ? cast(IColorFiller)rectangleFiller : cast(IColorFiller)ellipseFiller;
	jlogger.logf("Using filler with shape: %s", shape);

	PixelMerger merger = new PixelMerger(dbound, defaultSpliter, filler);
	jlogger.logf("Merging image with difference bound: %s", dbound);

	from = sw.peek();

	img = merger.merge(img);

	jlogger.info("Merging image use time: ", sw.peek() - from);

	if (show) img.show();

	from = sw.peek();
	if (ofn != "") {
		jlogger.logf("Image just write back to file %s", ofn);
		img.write(ofn);
		jlogger.info("Write in file use time: ", sw.peek() - from);
	} else {
		jlogger.logf("Write in stdout directly!!");
		img.write(stdout);
		jlogger.info("Write in stdout use time: ", sw.peek() - from);							
	}

}
