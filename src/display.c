#include <stdio.h>

#include <SDL2/SDL.h>

#define SDL_PREWORK(WIDTH, HEIGHT) \
	static int isInited = 0; \
	if (!isInited && SDL_Init(SDL_INIT_EVERYTHING) < 0) { \
		fprintf(stderr, "Error Init SDL: %s\n", SDL_GetError()); \
		return -1; \
	} isInited = 1; \
	SDL_Window *win; \
	SDL_Renderer *ren; \
	if (SDL_CreateWindowAndRenderer(WIDTH, HEIGHT, SDL_WINDOW_SHOWN | SDL_WINDOW_BORDERLESS, &win, &ren) < 0) { \
		fprintf(stderr, "Error create windows and renderer: %s\n", SDL_GetError()); \
		return -1; \
	} \
	SDL_Event e; \
	register int done = 0;

#define SDL_CLEANUP \
	SDL_DestroyRenderer(ren); \
	SDL_DestroyWindow(win);

#define SDL_MYEQUIT(FPS) \
	const int _delayTime = 1000 / FPS; \
	SDL_Delay(_delayTime); \
	while (SDL_PollEvent(&e)) { \
		if (e.type == SDL_QUIT) { \
			done = 1; break; \
		} else if (e.type == SDL_WINDOWEVENT) { \
		} else if (e.type == SDL_KEYUP) { \
			if (e.key.keysym.sym == SDLK_LEFT) { \
				int x, y; SDL_GetWindowPosition(win, &x, &y); \
				SDL_SetWindowPosition(win, x - 10, y); \
			} else if (e.key.keysym.sym == SDLK_RIGHT) { \
				int x, y; SDL_GetWindowPosition(win, &x, &y); \
				SDL_SetWindowPosition(win, x + 10, y); \
			} else if (e.key.keysym.sym == SDLK_UP) { \
				int x, y; SDL_GetWindowPosition(win, &x, &y); \
				SDL_SetWindowPosition(win, x, y - 10); \
			} else if (e.key.keysym.sym == SDLK_DOWN) { \
				int x, y; SDL_GetWindowPosition(win, &x, &y); \
				SDL_SetWindowPosition(win, x, y + 10); \
			} else { \
				done = 1; break; \
			} \
		} \
	}


int showColor(int R, int G, int B) {
	SDL_PREWORK(50, 50);
	while (!done) {
		SDL_MYEQUIT(60);

		if (SDL_SetRenderDrawColor(ren, R, G, B, 255) < 0) {
			fprintf(stderr, "Error Set render draw color: %s\n", SDL_GetError());
			return -1;
		}
		SDL_RenderFillRect(ren, NULL);
		SDL_RenderPresent(ren);
	}

	SDL_CLEANUP;
	return 0;
}

#define ItoRGB(c) (((c) >> 16) & 0xFF), (((c) >> 8) & 0xFF), ((c) & 0xFF)
int showImage(int h, int w, int *color) {
	double scale = 1.0;
	if (h > 1000 || w > 1900) {
		// need scale
		double sh = 1000.0 / h, sw = 1900.0 / w;
		scale = sh < sw ? sh : sw;
	}

	SDL_PREWORK(w * scale, h * scale);

	SDL_RenderSetLogicalSize(ren, w, h);
	// SDL_RenderSetIntegerScale(ren, SDL_TRUE);

	// Use Texture to generate the image
	SDL_Texture* text = SDL_CreateTexture(ren, SDL_PIXELFORMAT_RGB888, 
			SDL_TEXTUREACCESS_STREAMING, w, h);

	int pitch;
	Uint32 *pixels;
	SDL_LockTexture(text, NULL, (void **)&pixels, &pitch);
	for (int i = 0, len = h * w; i < len; ++i)
		pixels[i] = color[i];
	SDL_UnlockTexture(text);

	while (!done) {
		SDL_MYEQUIT(30);

		SDL_RenderClear(ren);
		SDL_RenderCopy(ren, text, NULL, NULL);
		SDL_RenderPresent(ren);
	} // event mainloop done.
	SDL_CLEANUP;
	return 0;
}


#ifdef TEST
int main(void) {
	showColor(5, 16, 255);
}
#endif
