/*
 * file: imgRW.cpp
 * This is a really bad implemention of reading and writing jpeg images.
 * */
#include <stdio.h>
#include <stdlib.h>

// Finally I introduce C++ here
#include <vector>

#include <jpeglib.h>

typedef unsigned int uint;

typedef struct ImgInfo {
	FILE *fp;

	uint width, height;
	uint components;
	
	struct jpeg_decompress_struct dinfo;
	struct jpeg_compress_struct cinfo;
	struct jpeg_error_mgr err;
} ImgInfo;
typedef int ImgInfo_t;

std::vector<ImgInfo> _img_pool;
std::vector<ImgInfo_t> _img_stack;

#ifdef __cplusplus
extern "C" {
#endif

ImgInfo_t newImgInfo() {
	static int use = 0;
	if (_img_stack.size()) {
		ImgInfo_t res = _img_stack.back();
		return _img_stack.pop_back(), res;
	}

	if (use == (int)_img_pool.size())
		_img_pool.resize(use ? use * 2 : 2); // simple stratage
	return use++;
}

void imgNoFree(ImgInfo_t imgID) {
	if (imgID < 0) return;
	ImgInfo *img = &_img_pool[imgID];
	img->fp = nullptr;
}

void imgClose(ImgInfo_t imgID) {
	if (imgID < 0) return;
	ImgInfo *img = &_img_pool[imgID];
	if (img->fp != nullptr) fclose(img->fp);
	_img_stack.push_back(imgID);
}

#define _FL_ __FILE__, __LINE__

ImgInfo_t imgWOpen(FILE *fp, int quality) {
	/*
	fprintf(stderr, "%s:%u: Opening JPEG file %s (write mode)\n", _FL_, fn);
	FILE *fp = fopen(fn, "wb");
	if (fp == NULL) {
		fprintf(stderr, "%s:%u: Error when open file %s (mode 'rb')\n", _FL_, fn);
		return -1;
	}
	*/

	ImgInfo_t imgID = newImgInfo();
	ImgInfo *img = &_img_pool[imgID];

	img->fp = fp;
	img->cinfo.err = jpeg_std_error(&img->err);
	struct jpeg_compress_struct *cinfo = &img->cinfo;

	jpeg_create_compress(cinfo);
	jpeg_stdio_dest(cinfo, fp);

	img->components = cinfo->input_components = 3;
	cinfo->in_color_space = JCS_RGB;

	jpeg_set_quality(cinfo, quality, TRUE);
	return imgID;
}

ImgInfo_t imgRaw(unsigned char *raw, int raw_size) {
	ImgInfo_t imgID = newImgInfo();
	ImgInfo *img = &_img_pool[imgID];

	img->fp = nullptr;
	img->dinfo.err = jpeg_std_error(&img->err);
	jpeg_create_decompress(&img->dinfo);

	jpeg_mem_src(&img->dinfo, raw, raw_size);
	jpeg_read_header(&img->dinfo, 1); // 1 for TRUE

	jpeg_start_decompress(&img->dinfo);
	img->width = img->dinfo.output_width;
	img->height = img->dinfo.output_height;
	img->components = img->dinfo.num_components;

	// fprintf(stderr, "%s:%u: Image width, height and components got: %d, %d, %d\n", _FL_, img->width, img->height, img->components);

	return imgID;
}

// Make sure info is allocated!!
// return id of the instance instead of it self !!
// return -1 if there's some error occurs.
ImgInfo_t imgROpen(FILE *fp) {
	ImgInfo_t imgID = newImgInfo();
	ImgInfo *img = &_img_pool[imgID];

	img->fp = fp;
	img->dinfo.err = jpeg_std_error(&img->err);
	jpeg_create_decompress(&img->dinfo);

	jpeg_stdio_src(&img->dinfo, fp);
	jpeg_read_header(&img->dinfo, 1); // 1 for TRUE

	jpeg_start_decompress(&img->dinfo);
	img->width = img->dinfo.output_width;
	img->height = img->dinfo.output_height;
	img->components = img->dinfo.num_components;

	// fprintf(stderr, "%s:%u: Image width, height and components got: %d, %d, %d\n", _FL_, img->width, img->height, img->components);

	return imgID;
}


uint imgWidth(const ImgInfo_t imgID) {
	return _img_pool[imgID].width;
}

uint imgHeight(const ImgInfo_t imgID) {
	return _img_pool[imgID].height;
}

uint imgComponents(const ImgInfo_t imgID) {
	return _img_pool[imgID].components;
}

int imgWPixels(const ImgInfo_t imgID, uint h, uint w, unsigned char *src) {
	if (src == NULL) {
		fprintf(stderr, "%s:%u: src is invalid!", _FL_);
		return -1;
	}

	ImgInfo *img = &_img_pool[imgID];
	struct jpeg_compress_struct *info = &img->cinfo;
	info->image_width = img->width = w, info->image_height = img->height = h;

	jpeg_set_defaults(info);
	jpeg_start_compress(info, TRUE);

	uint rowSize = w * img->components;

	unsigned char *buffer[1];
	while (info->next_scanline < h) {
		buffer[0] = &src[info->next_scanline * rowSize];
		(void) jpeg_write_scanlines(info, buffer, 1);
	}

	jpeg_finish_compress(info);
	jpeg_destroy_compress(info);

	// fprintf(stderr, "%s:%u: finished writing pixels (h %d x w %d)\n", _FL_, h, w);
	return 0;
}

// make sure dst is pre-allocated
// dst should have size of w*h*c bytes, or may cause segment false!
int imgRPixels(const ImgInfo_t imgID, unsigned char *dst) {
	if (dst == NULL) {
		fprintf(stderr, "%s:%u: dst is not allocated!\n", _FL_);
		return -1;
	}

	ImgInfo *img = &_img_pool[imgID];
	struct jpeg_decompress_struct *info = &img->dinfo;

	unsigned char *buffer[1];
	unsigned rowSize = img->width * img->components;
	while (info->output_scanline < img->height) {
		buffer[0] = &dst[info->output_scanline * rowSize];
		jpeg_read_scanlines(info, buffer, 1);
	}

	jpeg_finish_decompress(info);
	jpeg_destroy_decompress(info);

	return 0;
}

#ifdef TEST
int main(void) {
	const char *fn = "test-small.jpeg";
	ImgInfo_t img = imgRead(fn);
	if (img < 0) {
		fprintf(stderr, "%s:%u: Image read Failed!!\n", _FL_);
		return 1;
	}

	uint w = imgWidth(img), h = imgHeight(img), c = imgComponents(img);
	unsigned char * src = (unsigned char *)malloc(sizeof(unsigned char) * h * w * c);

	if (imgPixels(img, src) < 0) {
		fprintf(stderr, "%s:%u: Pixels read Error!!\n", _FL_);
		return 1;
	}

	for (unsigned i = 0; i < h; ++i) {
		unsigned offset = i * w * c;
		for (unsigned j = 0; j < w; ++j) {
			unsigned st = offset + j * 3;
			printf("(%u, %u, %u) ", src[st], src[st + 1], src[st + 2]);
		} puts("");
	}

	return 0;
}
#endif // TEST

#ifdef __cplusplus
} // end extern "C"
#endif
