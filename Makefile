DESTDIR ?= /usr/local/lib

# BUILD_DIR := ./build
BUILD_DIR := $(abspath ./build)
SRC_DIR := ./src

EXEC_FILES := $(SRC_DIR)/jimage.d $(SRC_DIR)/jmulti.d
EXEC := $(EXEC_FILES:$(SRC_DIR)/%.d=%)

JIMGSRC := $(shell find $(SRC_DIR)/jimg -name '*.d') $(shell find $(SRC_DIR) -name '*.c' -or -name '*.cpp')
JIMGOBJS := $(JIMGSRC:%=$(BUILD_DIR)/%.o)

JIMG_LIB := $(BUILD_DIR)/libjimg.so

LIB_DEP := $(shell pkg-config sdl2 libjpeg --libs) -lstdc++
CFLAGS := $(shell pkg-config sdl2 libjpeg  --cflags) -Wall -Wextra -fPIC -O3 $(if $(DEBUG),-g,)
DFLAGS := -O$(if $(DEBUG), -g,)$(if $(COV), -cov,)

.PHONY: ALL
all: exec $(JIMG_LIB)

exec: $(EXEC_FILES) $(JIMG_LIB)
	$(foreach target, $(EXEC), dmd -of=$(target) $(SRC_DIR)/$(target).d -I$(SRC_DIR) -L-ljimg -L-rpath=$(BUILD_DIR) -L-L$(BUILD_DIR) $(DFLAGS);)

$(JIMG_LIB): $(JIMGOBJS)
	@# dmd -shared -of=$@ -defaultlib=libphobos2.so $(JIMGOBJS) $(LIB_DEP:%=-L%) $(DFLAGS)
	gcc -shared -o $@ -defaultlib=libphobos2.so $(JIMGOBJS) $(LIB_DEP) $(CFLAGS)

$(BUILD_DIR)/%.d.o: %.d
	mkdir -p $(dir $@)
	dmd -c $< -of=$@ -I$(SRC_DIR) $(DFLAGS)

$(BUILD_DIR)/%.c.o: %.c
	mkdir -p $(dir $@)
	gcc -c $< -o $@ $(CFLAGS)

$(BUILD_DIR)/%.cpp.o: %.cpp
	mkdir -p $(dir $@)
	g++ -c $< -o $@ $(CFLAGS)

.PHONY: install
install:
	cp -n $(JIMG_LIB) $(DESTDIR)

.PHONY: clean
clean:
	rm -rf build/*
	rm -rf $(EXEC) $(EXEC:%=%.o)

test/proc: $(JIMG_LIB) test/procFrames.d
	dmd -of=$@ $^ -I$(SRC_DIR) -L-L$(BUILD_DIR) -L-rpath=$(BUILD_DIR) -L-ljimg $(DFLAGS)
