#!/bin/bash

echo -n 'Please enter a video you want to proceed: '
read file

echo -n 'Please enter the filename you want to save: '
read outfile

make

mkdir -p frames
rm -rf frames/*-merged*
ffmpeg -i $file frames/frame%03d.jpeg
./jmulti -b 3 frames none

frameRate=$(ffprobe -v error -count_frames -select_streams v:0 -show_entries stream=r_frame_rate -of default=nokey=1:noprint_wrappers=1 $file)
ffmpeg -i $file -r $frameRate -f image2 -i frames/frame%03d-merged.jpeg  -c copy -map 0:a -map 1:v $outfile
rm -rf $outfile.tmp.mp4
